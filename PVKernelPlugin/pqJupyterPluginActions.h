#ifndef pqJupyterPluginActions_h
#define pqJupyterPluginActions_h

#include <QActionGroup>

class pqJupyterPluginActions : public QActionGroup
{
  Q_OBJECT
public:
  pqJupyterPluginActions(QObject* p);
  ~pqJupyterPluginActions() = default;
};

#endif
