import numpy
from paraview.simple import *
from paraview import vtk

# *****************
# Pipeline
# *****************
# -----------------------------------------------------------------------------
def _CreatePipelineSource(grid, name='MyGrid', representation='Surface'):
    """ Create a data producer containg the input vtk grid as data.
    Add it to the pipeline and show it. By default, use the Surface representation.
    Works only on builtin server.
    """

    producer = PVTrivialProducer(guiName=name)
    cso = producer.GetClientSideObject()
    cso.SetOutput(grid)
    rep = Show(producer)
    rep.Representation = representation
    Render()
    return producer

# -----------------------------------------------------------------------------
def CreateRegularGrid(extent, name, origin = [0, 0, 0], spacing = [1, 1, 1]):
    """Create a pipeline source with a vtkImageData.
    Works only on builtin server.
    """

    from paraview import vtk

    grid = vtk.vtkImageData()
    grid.SetExtent(extent)
    grid.SetOrigin(origin)
    grid.SetSpacing(spacing)
    return _CreatePipelineSource(grid, name)

# -----------------------------------------------------------------------------
def CreateRegularGridFromData(numpyData, name="MyGrid", dataName="MyDataArray"):
    """Create a producer with a vtkImageData.
    Works only on builtin server.
    """

    s = numpyData.shape
    if len(s) != 3:
        print("data shape not supported, should have 3 dimensions. Please try using `CreateRegularGrid()` and then `AddArray()`.")
        return

    extent = [0, s[0] - 1, 0, s[1] - 1, 0, s[2] - 1]
    grid = CreateRegularGrid(extent, name)
    AddPointArray(numpyData, dataName, grid)
    ColorByArray(dataName)

    return grid

# -----------------------------------------------------------------------------
def CreateUnstructuredGrid(numpyPoints, numpyCells, cellTypes = None, name="MyGrid"):
    """Create a producer with a vtkUnstructuredGrid. Works only on builtin server.

    numpyPoints: a numpy array containg the points
    numpyCells: a numpy array containg the cells
    cellTypes: the cell types. Can be either :
     - a list of vtkCellType values, one for each specified cell.
     - a single vtkCellType value used for each cell.
     - None, falling back to HEXAHEDRON.
    name: the name in the pipelineBrowser.
    """

    points = vtk.vtkPoints()
    for i in range(len(numpyPoints)):
        points.InsertPoint(i, numpyPoints[i])

    ugrid = vtk.vtkUnstructuredGrid()
    ugrid.SetPoints(points)

    defaultType = -1
    defaultSize = 1
    # if type is not specified, use HEXAHEDRON
    if cellTypes == None:
        defaultType = vtk.VTK_HEXAHEDRON
        defaultSize = 8
    # if only one type is give, use the same for each cell
    elif isinstance(cellTypes, int) :
        defaultType = cellTypes
        defaultSize = len(numpyCells[0])

    numberOfCells = len(numpyCells)
    ugrid.AllocateEstimate(numberOfCells, defaultSize)

    for i in range(numberOfCells):
        ct = cellTypes[i] if defaultType == -1 else defaultType
        ugrid.InsertNextCell(ct, len(numpyCells[i]), numpyCells[i])

    return _CreatePipelineSource(ugrid, name)

# -----------------------------------------------------------------------------
def RemoveFromPipeline(source=None):
    """ Remove the given source from the pipeline. This source should not have any children.
    """
    if not source:
        source = GetActiveSource()
    if not source:
        raise RuntimeError ("Could not locate source to remove")

    for cc in range(source.SMProxy.GetNumberOfConsumers()):
        consumer = source.SMProxy.GetConsumerProxy(cc)
        # do not remove sources that have children
        if consumer in GetSources().values():
            print('Cannot remove a source that have children.')
            return

    Delete(source)
    del source

# *****************
# Data Array
# *****************
# -----------------------------------------------------------------------------
def AddPointArray(data, name="MyDataArray", proxy=None):
    AddArray(data, name, proxy, "POINT")

# -----------------------------------------------------------------------------
def AddCellArray(data, name="MyDataArray", proxy=None):
    AddArray(data, name, proxy, "CELL")

# -----------------------------------------------------------------------------
def AddArray(data, name, proxy, association):
    """ Require a numpy array as data.

    If an array with the same name already exists in data structure,
    then the new one will replace it.
    The data shape should match the grid.
    """
    from paraview.vtk.util import numpy_support

    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'AddArray' on")

    cso = proxy.GetClientSideObject()
    grid = cso.GetOutputDataObject(0)
    array = numpy.empty(0)
    try:
        nbOfTuples = grid.GetNumberOfPoints() if association == "POINT" else grid.GetNumberOfCells()
        array = numpy.reshape(data, (nbOfTuples, -1), order="F")
    except:
        print ("Array dimensions does not match the grid !")
        return

    # VTK_data has shape (nbPoints, nbOfComponents)
    VTK_data = numpy_support.numpy_to_vtk(num_array=array)
    VTK_data.SetName(name)
    fieldata = grid.GetFieldData()
    if association == "POINT":
        fieldata = grid.GetPointData()
    elif association == "CELL":
        fieldata = grid.GetCellData()

    fieldata.AddArray(VTK_data)
    fieldata.SetActiveScalars(name)
    proxy.MarkModified(None)
    proxy.UpdatePipeline()

# -----------------------------------------------------------------------------
def RemoveArray(id, proxy=None):
    """ Remove the array corresponding to id.
    id can be either the name or the index of the array.
    """
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'RemoveArray' on")

    grid = proxy.GetClientSideObject().GetOutputDataObject(0)
    grid.GetPointData().RemoveArray(id)

# -----------------------------------------------------------------------------
def ColorByArray(arrayName, proxy=None):
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'AddArray' on")

    rep = Show(proxy)
    ColorBy(rep, arrayName)
    UpdateScalarBars()
    Render()

# *****************
# Selection
# *****************
# -----------------------------------------------------------------------------
def GetSelection(proxy=None, port=0):
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'GetSelection' on")

    sel = proxy.GetSelectionInput(port)
    if sel == None:
        return ["", []]

    field = proxy.GetSelectionInput(port).FieldType
    ids = proxy.GetSelectionInput(port).IDs
    # IDs is on format [0, id0, 0, id1, ..., 0, idn]
    ids = [ids[i] for i in range(1, len(ids), 2)]
    return [field, ids]

# -----------------------------------------------------------------------------
def GetSelectedCells(proxy=None, port=0):
    sel = GetSelection(proxy, port)
    return sel[1] if sel[0] == "CELL" else []

# -----------------------------------------------------------------------------
def GetSelectedPoints(proxy=None, port=0):
    sel = GetSelection(proxy, port)
    return sel[1] if sel[0] == "POINT" else []


# *****************
# Display
# *****************
# -----------------------------------------------------------------------------
def Display(w = 400, h = 300, all_views = False):
    ''' Dummy declaration for autocompletion purpose. It is handled in cpp code.
    see also PVInterpreter.cxx
    '''
    pass
